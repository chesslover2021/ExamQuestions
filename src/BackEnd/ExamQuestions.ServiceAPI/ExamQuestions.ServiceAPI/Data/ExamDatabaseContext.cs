﻿using ExamQuestions.ServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace ExamQuestions.ServiceAPI.Data;

public partial class ExamDatabaseContext : DbContext
{
    public ExamDatabaseContext(DbContextOptions<ExamDatabaseContext> options) : base(options)
    {
    }

    public virtual DbSet<ExamQuestion> ExamQuestions { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ExamQuestion>(entity =>
        {
            entity.ToTable("ExamQuestion");

            entity.Property(e => e.Id).UseIdentityColumn();
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}