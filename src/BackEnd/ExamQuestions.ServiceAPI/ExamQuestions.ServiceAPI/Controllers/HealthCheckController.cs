using Microsoft.AspNetCore.Mvc;

namespace ExamQuestions.ServiceAPI.Controllers;

[Route("healthcheck")]
[ApiController]
public class HealthCheckController : ControllerBase
{
    [HttpGet]
    public IActionResult Get()
    {
        return Ok(new { message = "Exam Questions Service API", status = "Healthy" });
    }
}