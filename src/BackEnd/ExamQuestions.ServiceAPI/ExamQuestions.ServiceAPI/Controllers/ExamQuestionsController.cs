using ExamQuestions.ServiceAPI.Models;
using ExamQuestions.ServiceAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExamQuestions.ServiceAPI.Controllers;

[Route("examquestions")]
[ApiController]
public class ExamQuestionsController : ControllerBase
{
    private readonly ILogger<ExamQuestionsController> _logger;
    private readonly ExamQuestionsService _service;

    public ExamQuestionsController(ILogger<ExamQuestionsController> logger, ExamQuestionsService service)
    {
        _logger = logger;
        _service = service;
    }

    [HttpGet("/")]
    public async Task<IQueryable<ExamQuestion>> Get()
    {
        return (await _service.GetExamQuestions()).AsNoTracking();
    }

    [HttpPost("/")]
    public async Task<ActionResult<ExamQuestion>> Post(ExamQuestion examQuestion)
    {
        try
        {
            examQuestion = await _service.AddExamQuestion(examQuestion);
            return CreatedAtAction("POST", examQuestion);
        }
        catch (Exception exception)
        {
            return BadRequest(exception.StackTrace);
        }
    }

    [HttpGet("/{id}")]
    public async Task<ActionResult<ExamQuestion>> Get(int id)
    {
        try
        {
            var examQuestion = await _service.GetExamQuestion(id);
            if (examQuestion != null)
            {
                return Ok(examQuestion);
            }
        }
        catch (Exception exception)
        {
            return BadRequest(exception.Message);
        }
        return NotFound();
    }

    [HttpPut("/")]
    public async Task<ActionResult<ExamQuestion>> Put(ExamQuestion examQuestion)
    {
        try
        {
            await _service.UpdateExamQuestion(examQuestion);
        }
        catch (Exception exception)
        {
            return BadRequest(exception.Message);
        }
        return NoContent();
    }

    [HttpDelete("/{id}")]
    public async Task<ActionResult<ExamQuestion>> Delete(int id)
    {
        try
        {
            var examQuestion = await _service.DeleteExamQuestion(id);
            if (examQuestion == null)
            {
                return NotFound(id);
            }
        }
        catch (Exception exception)
        {
            return BadRequest(exception.StackTrace);
        }
        return NoContent();
    }
}
