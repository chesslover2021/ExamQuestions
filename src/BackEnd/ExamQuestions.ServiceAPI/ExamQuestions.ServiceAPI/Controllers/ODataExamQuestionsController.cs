using ExamQuestions.ServiceAPI.Models;
using ExamQuestions.ServiceAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;

namespace ExamQuestions.ServiceAPI.Controllers;

// public class ODataExamQuestionsController : ODataController
// {
//     private readonly ILogger<ODataExamQuestionsController> _logger;
//     private readonly ExamQuestionsService _service;
//
//     public ODataExamQuestionsController(ILogger<ODataExamQuestionsController> logger, ExamQuestionsService service)
//     {
//         _logger = logger;
//         _service = service;
//     }
//
//     [HttpGet("odata/examquestions")]
//     public async Task<IQueryable<ExamQuestion>> OData()
//     {
//         return (await _service.GetExamQuestions()).AsNoTracking();
//     }
// }