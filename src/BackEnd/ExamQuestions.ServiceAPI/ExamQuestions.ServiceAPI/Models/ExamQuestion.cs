﻿namespace ExamQuestions.ServiceAPI.Models;

public partial class ExamQuestion
{
    public long Id { get; set; }
    public string Question { get; set; } = null!;
    public string Answer { get; set; } = null!;
}