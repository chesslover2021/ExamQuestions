using ExamQuestions.ServiceAPI.Data;
using ExamQuestions.ServiceAPI.Models;
using ExamQuestions.ServiceAPI.Services;
using Microsoft.AspNetCore.OData;
using Microsoft.EntityFrameworkCore;
using Microsoft.OData.ModelBuilder;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<ExamDatabaseContext>(options => options.UseSqlServer(builder.Configuration.GetSection("Database")["ConnectionString"]));
builder.Services.AddScoped<ExamQuestionsRepository>();
builder.Services.AddScoped<ExamQuestionsService>();

// builder.Services.AddControllers().AddOData(opt => {
//     ODataConventionModelBuilder odataBuilder = new ODataConventionModelBuilder();
//     odataBuilder.EntitySet<ExamQuestion>("ExamQuestions");
//     odataBuilder.EnableLowerCamelCase();
//     opt.Expand().Count().Select().OrderBy().SkipToken().Filter().SetMaxTop(100);
//
//     opt.AddRouteComponents("odata", odataBuilder.GetEdmModel());
// });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
