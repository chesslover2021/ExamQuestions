﻿using ExamQuestions.ServiceAPI.Data;
using ExamQuestions.ServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace ExamQuestions.ServiceAPI.Services;

public class ExamQuestionsRepository
{
    private readonly ExamDatabaseContext _context;

    public ExamQuestionsRepository(ExamDatabaseContext context)
    {
        _context = context;
    }

    public async Task<IQueryable<ExamQuestion>> GetExamQuestions()
    {
        return (await _context.ExamQuestions.AsNoTracking().ToListAsync()).AsQueryable();
    }

    public async Task<ExamQuestion> GetExamQuestion(int id)
    {
        return await _context.ExamQuestions.FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<ExamQuestion> AddExamQuestion(ExamQuestion examQuestion)
    {
        var result = await _context.ExamQuestions.AddAsync(examQuestion);
        await _context.SaveChangesAsync();
        return result.Entity;
    }

    public async Task<ExamQuestion> UpdateExamQuestion(ExamQuestion examQuestion)
    {
        var result = await _context.ExamQuestions.FirstOrDefaultAsync(e => e.Id == examQuestion.Id);
        if (result != null)
        {
            result.Answer = examQuestion.Answer;
            result.Question = examQuestion.Question;
            await _context.SaveChangesAsync();
            return result;
        }
        return null;
    }

    public async Task<ExamQuestion> DeleteExamQuestion(int id)
    {
        var result = await _context.ExamQuestions.FirstOrDefaultAsync(e => e.Id == id);
        if (result != null)
        {
            result = _context.ExamQuestions.Remove(result).Entity;
            await _context.SaveChangesAsync();
        }

        return result;
    }
}