﻿using ExamQuestions.ServiceAPI.Models;

namespace ExamQuestions.ServiceAPI.Services;

public class ExamQuestionsService
{
    private readonly ExamQuestionsRepository _repo;

    public ExamQuestionsService(ExamQuestionsRepository repo)
    {
        _repo = repo;
    }

    public Task<IQueryable<ExamQuestion>> GetExamQuestions()
    {
        return _repo.GetExamQuestions();
    }

    public Task<ExamQuestion> GetExamQuestion(int id)
    {
        return _repo.GetExamQuestion(id);
    }

    public Task<ExamQuestion> AddExamQuestion(ExamQuestion examQuestion)
    {
        return _repo.AddExamQuestion(examQuestion);
    }

    public Task<ExamQuestion> UpdateExamQuestion(ExamQuestion examQuestion)
    {
        return _repo.UpdateExamQuestion(examQuestion);
    }

    public Task<ExamQuestion> DeleteExamQuestion(int id)
    {
        return _repo.DeleteExamQuestion(id);
    }
}