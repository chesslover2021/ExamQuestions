import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    rollupOptions: {
      output: {
        entryFileNames: `assets/app/exam-questions-app/cm-employee-app.js`,
        chunkFileNames: `assets/app/assets/vendor.js`,
        assetFileNames: (assetInfo) => {
          if (assetInfo.name == 'style.css')
            return `assets/app/exam-questions-app/cm-employee-app.css`;
          return `assets/[name].[ext]`;
        },
        manualChunks: undefined
      },
      treeshake: false
    },
    cssCodeSplit: false
  }
})
