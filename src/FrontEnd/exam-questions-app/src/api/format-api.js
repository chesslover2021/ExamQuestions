import { formatMessage as localizationMessage } from 'devextreme/localization';

/**
 * Format message with key as fallback value
 * 
 */
export const formatMessage=(key, value)=>{
    let message = localizationMessage(key,value);
    if(message===''){
        return key;
    }
    return message;
}