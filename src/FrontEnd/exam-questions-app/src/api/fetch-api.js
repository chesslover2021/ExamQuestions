export const get = async (accessToken, url, query) => {
    let response = await fetch(`${url}?${new URLSearchParams(query)}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`,
        }
    });

    return response.json();
}